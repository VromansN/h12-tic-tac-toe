class Player:
    def __init__(self, name: str, number: int, symbol: str, won: int, color: tuple):
        self._name = name
        self._number = number
        self._symbol = symbol
        self._won = won
        self._color = color

    @property
    def get_name(self) -> str:
        return self._name

    @property
    def get_number(self) -> int:
        return self._number

    @property
    def get_symbol(self) -> str:
        return self._symbol

    @property
    def get_games_won(self) -> int:
        return self._won

    @property
    def get_color(self) -> tuple:
        return self._color

    def make_move(self, game: 'Game', btn: 'GameButton', ids: 'ObservableDict') -> None:
        game.make_move(btn, ids, self)

    def add_won(self, amount_to_add: int = 1) -> None:
        self._won += amount_to_add

    def reset_won(self) -> None:
        self._won = 0

    def as_dict(self) -> dict:
        return {
            'name': self._name,
            'number': self._number,
            'symbol': self._symbol,
            'won': self._won,
            'color': self._color,
        }
