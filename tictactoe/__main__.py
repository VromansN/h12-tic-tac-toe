from kivy.app import App
from tictactoe.view.game_view import GameView


class TicTacToeApp(App):
    title = 'Tic-Tac-Toe'

    def build(self) -> GameView:
        return GameView()


if __name__ == '__main__':
    TicTacToeApp().run()
