from tictactoe.game.config import GameConfig


class Game:
    button_to_grid_mapping = {
        1: {
            'id': 'game_btn_1',
            'grid_index': [0, 0],
        },
        2: {
            'id': 'game_btn_2',
            'grid_index': [0, 1],
        },
        3: {
            'id': 'game_btn_3',
            'grid_index': [0, 2],
        },
        4: {
            'id': 'game_btn_4',
            'grid_index': [1, 0],
        },
        5: {
            'id': 'game_btn_5',
            'grid_index': [1, 1],
        },
        6: {
            'id': 'game_btn_6',
            'grid_index': [1, 2],
        },
        7: {
            'id': 'game_btn_7',
            'grid_index': [2, 0],
        },
        8: {
            'id': 'game_btn_8',
            'grid_index': [2, 1],
        },
        9: {
            'id': 'game_btn_9',
            'grid_index': [2, 2],
        },
    }

    def __init__(self):
        self._game_config = GameConfig()
        self._is_start = True
        self._is_finished = False
        self._has_winner = False
        self._players = []
        self._current_player = None
        self._winner = None
        self._total_games_played = self._game_config.get_total_games_played()
        self._current_player_index = 0
        self._moves_made = 0
        self._moves = [[[], [], [], ], [[], [], [], ], [[], [], [], ], ]

        self.add_players(self._game_config.get_players(), self._game_config.get_current_player())

    @property
    def is_start(self) -> bool:
        return self._is_start

    @property
    def is_finished(self) -> bool:
        return self._is_finished

    @property
    def has_winner(self) -> bool:
        return self._has_winner

    @property
    def get_winner(self) -> 'Player' or None:
        return self._winner

    def _add_player(self, player: 'Player') -> None:
        self._players.append(player)

    def add_players(self, players: list, current_player: str) -> None:
        for index, player in enumerate(players):
            self._add_player(player)

            if player.get_name == current_player:
                self._current_player = player
                self._current_player_index = index

    def get_players(self) -> list:
        return self._players

    @property
    def get_current_player(self) -> 'Player':
        return self._current_player

    def next_player(self) -> 'Player':
        self._current_player_index += 1

        if self._current_player_index >= len(self._players):
            self._current_player_index = 0

        self._current_player = self._players[self._current_player_index]

        return self._current_player

    @property
    def get_total_games_played(self) -> int:
        return self._total_games_played

    def reset_total_games_played(self) -> None:
        self._total_games_played = 0

    def increment_total_games_played(self, amount_to_increment_by: int = 1) -> None:
        self._total_games_played += amount_to_increment_by

    def finish_game(self) -> None:
        self._is_finished = True

    def start_game(self) -> None:
        self._is_start = True

    def check_win_lines(self, player: 'Player') -> None:
        moves = self._moves
        row1 = isinstance(moves[0][0], str) and moves[0][0] == moves[0][1] and moves[0][1] == moves[0][2]
        row2 = isinstance(moves[1][0], str) and moves[1][0] == moves[1][1] and moves[1][1] == moves[1][2]
        row3 = isinstance(moves[2][0], str) and moves[2][0] == moves[2][1] and moves[2][1] == moves[2][2]
        col1 = isinstance(moves[0][0], str) and moves[0][0] == moves[1][0] and moves[1][0] == moves[2][0]
        col2 = isinstance(moves[0][1], str) and moves[0][1] == moves[1][1] and moves[1][1] == moves[2][1]
        col3 = isinstance(moves[0][2], str) and moves[0][2] == moves[1][2] and moves[1][2] == moves[2][2]
        diagonal1 = isinstance(moves[0][0], str) and moves[0][0] == moves[1][1] and moves[1][1] == moves[2][2]
        diagonal2 = isinstance(moves[2][0], str) and moves[2][0] == moves[1][1] and moves[1][1] == moves[0][2]

        if row1 or row2 or row3 or col1 or col2 or col3 or diagonal1 or diagonal2:
            self._has_winner = True
            self._is_finished = True
            self._winner = player

        if self._moves_made >= len(self.button_to_grid_mapping):
            self._is_finished = True

    def make_move(self, btn: 'GameButton', ids: 'ObservableDict', player: 'Player') -> None:
        total_possible_moves = len(self.button_to_grid_mapping)

        for _ in range(1, total_possible_moves + 1):
            if btn == ids[self.button_to_grid_mapping[_]['id']]:
                self._moves_made += 1
                row, col = self.button_to_grid_mapping[_]['grid_index']
                self._moves[row][col] = player.get_symbol

                break

        self.check_win_lines(player)

    def reset_config(self) -> None:
        self._game_config.write_default_config_data()

    def update_config(self) -> None:
        players = []

        for player in self._players:
            players.append(player.as_dict())

        data = {
            self._game_config.TOTAL_GAMES_PLAYED_KEY: self._total_games_played,
            self._game_config.CURRENT_PLAYER_KEY: self._current_player.get_name,
            self._game_config.PLAYERS_KEY: players,
        }

        self._game_config.update_game_config(data)
