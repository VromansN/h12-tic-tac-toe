import sys
import yaml
from pathlib import Path
from kivy.config import Config
from tictactoe.player.player import Player


class GameConfig:
    CURRENT_PLAYER_KEY = 'current_player'
    TOTAL_GAMES_PLAYED_KEY = 'total_games_played'
    PLAYERS_KEY = 'players'
    PLAYER_NUMBER_KEY = 'number'
    PLAYER_NAME_KEY = 'name'
    PLAYER_SYMBOL_KEY = 'symbol'
    PLAYER_WON_KEY = 'won'
    COLOR_KEY = 'color'
    PLAYER_1_NAME = 'Player 1'
    PLAYER_1_SYMBOL = 'O'
    PLAYER_2_NAME = 'Player 2'
    PLAYER_2_SYMBOL = 'X'
    DEFAULT_DATA = {
        CURRENT_PLAYER_KEY: 'Player 1',
        TOTAL_GAMES_PLAYED_KEY: 0,
        PLAYERS_KEY: [
            {
                PLAYER_NUMBER_KEY: 1,
                PLAYER_NAME_KEY: PLAYER_1_NAME,
                PLAYER_SYMBOL_KEY: PLAYER_1_SYMBOL,
                PLAYER_WON_KEY: 0,
                COLOR_KEY: (0, 1, 0, 1),
            },
            {
                PLAYER_NUMBER_KEY: 2,
                PLAYER_NAME_KEY: PLAYER_2_NAME,
                PLAYER_SYMBOL_KEY: PLAYER_2_SYMBOL,
                PLAYER_WON_KEY: 0,
                COLOR_KEY: (0, 0, 1, 1),
            }
        ]
    }

    _game_config = {}

    def __init__(self):
        Config.set('graphics', 'fullscreen', 0)
        Config.set('graphics', 'resizable', 0)
        Config.set('graphics', 'height', 800)
        Config.set('graphics', 'width', 600)
        Config.set('kivy', 'exit_on_escape', 0)
        Config.set('input', 'mouse', 'mouse,multitouch_on_demand')
        Config.write()

    @staticmethod
    def get_config_file() -> Path:
        return Path('game/config.yaml')

    def get_config_file_data(self) -> dict:
        config_file = self.get_config_file()

        if config_file.exists():
            with open(config_file, 'r') as stream:
                try:
                    return yaml.safe_load(stream)
                except yaml.YAMLError:
                    self.write_default_config_data()
                    return self.get_config_file_data()
                except Exception as err:
                    sys.exit(err)
        else:
            self.write_default_config_data()
            return self.get_config_file_data()

    def write_default_config_data(self) -> None:
        with open(self.get_config_file(), 'w') as stream:
            yaml.safe_dump(self.DEFAULT_DATA, stream)

    def get_config_data(self, key: str) -> any:
        config_data = self.get_config_file_data()

        if isinstance(config_data, dict) and key in config_data:
            return config_data[key]
        else:
            self.write_default_config_data()

            return self.get_config_data(key)

    def get_current_player(self) -> str:
        return self.get_config_data('current_player')

    def get_players(self) -> list:
        players = []
        players_in_config = self.get_config_data('players')

        try:
            for player_in_config in players_in_config:
                name = player_in_config[self.PLAYER_NAME_KEY]
                number = player_in_config[self.PLAYER_NUMBER_KEY]
                symbol = player_in_config[self.PLAYER_SYMBOL_KEY]
                won = player_in_config[self.PLAYER_WON_KEY]
                color = player_in_config[self.COLOR_KEY]

                players.append(Player(name, number, symbol, won, color))
        except:
            self.write_default_config_data()
            return self.get_players()

        return players

    def get_total_games_played(self) -> any:
        return self.get_config_data(self.TOTAL_GAMES_PLAYED_KEY)

    def update_game_config(self, data: dict) -> None:
        config = self.get_config_file_data()

        for key, value in data.items():
            config[key] = value

        with open(self.get_config_file(), 'w') as stream:
            yaml.safe_dump(config, stream)
