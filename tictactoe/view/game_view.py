from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from tictactoe.game.game import Game


class GameView(BoxLayout):
    lbn_current_player = ObjectProperty(None)
    lbn_total_won = ObjectProperty(None)
    btn_reset = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(GameView, self).__init__(**kwargs)
        self.start_new_game()

    def start_new_game(self, btn=None) -> None:
        self.game = Game()
        self.reset_buttons(self.game)
        self.update_total_count()

        current_player = self.game.get_current_player

        self.lbn_current_player.text = f'Player {current_player.get_number} ({current_player.get_symbol})'

    def reset_buttons(self, game: Game) -> None:
        for _ in range(1, len(game.button_to_grid_mapping) + 1):
            self.ids[game.button_to_grid_mapping[_]['id']].text = ''
            self.ids[game.button_to_grid_mapping[_]['id']].disabled = False

    def update_total_count(self) -> None:
        self.lbn_total_won.text = ''
        message = []

        for player in self.game.get_players():
            message.append(
                f'Player {player.get_number} ({player.get_symbol}): {player.get_games_won}/{self.game.get_total_games_played}')

        self.lbn_total_won.text += '\n'.join(message)

    def reset_config(self, btn=None) -> None:
        self.game.reset_config()

    def stop_playing(self, btn=None) -> None:
        App.get_running_app().stop()

    def game_button_release(self, btn) -> None:
        player = self.game.get_current_player

        if not self.game.is_finished:
            btn.text = player.get_symbol
            btn.disabled = True
            # todo: make the color work across rounds
            # btn.color = player.get_color

            player.make_move(self.game, btn, self.ids)

            if self.game.is_finished:
                self.game.increment_total_games_played()
                box_layout = BoxLayout(orientation='vertical')
                game_over_dialog = Popup(
                    title='Game Over',
                    size_hint=(None, None),
                    size=(300, 300),
                    auto_dismiss=False,
                )

                lbl_winner = Label(text='')

                if self.game.has_winner:
                    winner = self.game.get_winner

                    lbl_winner.text += f'Winner: Player {winner.get_number} ({winner.get_symbol})'
                    player.add_won()
                else:
                    lbl_winner.text += 'It\'s a draw'

                self.game.next_player()
                self.game.update_config()

                btn_restart = Button(text='Play again.')
                btn_restart.bind(on_press=self.start_new_game)
                btn_restart.bind(on_release=game_over_dialog.dismiss)

                btn_stop = Button(text='Stop playing.')
                btn_stop.bind(on_press=self.reset_config)
                btn_stop.bind(on_release=self.stop_playing)

                box_layout.add_widget(lbl_winner)
                box_layout.add_widget(btn_restart)
                box_layout.add_widget(btn_stop)
                game_over_dialog.content = box_layout

                game_over_dialog.open()

                return

            next_player = self.game.next_player()
            self.lbn_current_player.text = f'Player {next_player.get_number} ({next_player.get_symbol})'

    def reset_total_count(self) -> None:
        self.game.reset_total_games_played()

        for player in self.game.get_players():
            player.reset_won()

        self.game.update_config()
        self.update_total_count()
