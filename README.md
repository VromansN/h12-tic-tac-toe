# H12 - Tic-Tac-Toe game
To get started you must first install the dependencies: `python3 -m pip install -r requirements.txt`
(use [Python 3.7](https://www.python.org/downloads/release/python-377/), Kivy doesn't yet work with Python 3.8).

To start playing, simply run `python3 -m tictactoe`.
